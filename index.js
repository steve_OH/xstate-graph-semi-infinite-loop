import Graph from '@xstate/graph';
import { machine } from './machine.js';

const { getSimplePaths } = Graph;
const graph = getSimplePaths(machine);

console.dir(graph, { depth: null });
