import XState from 'xstate';

const { assign, createMachine } = XState;

export const machine = createMachine({
  id: 'test-machine',
  context: {
    beta: 0,
    alpha: 0,
  },
  initial: 'A',
  states: {
    A: {
      on: {
        NEXT: {
          target: 'B',
        },
      },
    },
    B: {
      type: 'parallel',
      on: {
        BACK: {
          target: 'A',
        },
      },
      states: {
        alpha: {
          initial: 'zero',
          states: {
            zero: {
              on: {
                SET_ALPHA_ONE: {
                  target: 'one',
                  actions: assign({ alpha: 1 }),
                },
              },
            },
            one: {
              on: {
                SET_ALPHA_ZERO: {
                  target: 'zero',
                  actions: assign({ alpha: 0 }),
                },
              },
            },
          },
        },
        beta: {
          initial: 'zero',
          states: {
            zero: {
              on: {
                SET_BETA_ONE: {
                  target: 'one',
                  actions: assign({ beta: 1 }),
                },
                SET_BETA_TWO: {
                  target: 'two',
                  actions: assign({ beta: 2 }),
                },
              },
            },
            one: {
              on: {
                SET_BETA_TWO: {
                  target: 'two',
                  actions: assign({ beta: 2 }),
                },
              },
            },
            two: {
              on: {
                SET_BETA_ONE: {
                  target: 'one',
                  actions: assign({ beta: 1 }),
                },
              },
            },
          },
        },
      },
    },
  },
});
